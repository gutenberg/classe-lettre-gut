while IFS= read -r font; do
    echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    fontnospace=${font//[[:blank:]]/}
    lualatex -interaction=nonstopmode "\newcommand{\thefont}{${font}}\input{banniere}"  2>&1 > /dev/null
    if rg -q "LaTeX3 error" banniere.log
    then
        echo "Fonte '$font' : erreur"
    else
        if rg -q "LaTeX3 error" banniere.log
        then
            echo "Fonte '$font' : erreur"
        else
            if rg -q "Some font shapes were not available, defaults substituted." banniere.log
            then
                echo "Fonte '$font' : sous optimal"
                cp -f banniere.pdf sous-optimal/${fontnospace}.pdf
            else
                echo "Fonte '$font' : OK"
                cp -f banniere.pdf OK/${fontnospace}.pdf
            fi
        fi
    fi
    # latexmk -jobname=${fontnospace} -pdflatex="lualatex %O '\newcommand{\thefont}{${font}}\input{%S}'" banniere
done < liste-opentype-fonts-regular.txt
